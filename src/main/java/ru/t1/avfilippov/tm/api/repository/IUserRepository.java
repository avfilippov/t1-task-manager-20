package ru.t1.avfilippov.tm.api.repository;

import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.model.User;

import javax.jws.soap.SOAPBinding;
import java.util.List;

public interface IUserRepository extends IRepository<User>{

    User create (String login, String password);

    User create (String login, String password, String email);

    User create (String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
