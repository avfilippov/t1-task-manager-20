package ru.t1.avfilippov.tm.api.model;

import ru.t1.avfilippov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
