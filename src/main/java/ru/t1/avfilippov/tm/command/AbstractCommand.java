package ru.t1.avfilippov.tm.command;

import ru.t1.avfilippov.tm.api.model.ICommand;
import ru.t1.avfilippov.tm.api.service.IAuthService;
import ru.t1.avfilippov.tm.api.service.IServiceLocator;
import ru.t1.avfilippov.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract String getName();

    public abstract Role[] getRoles();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void  setServiceLocator (IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public IAuthService getAuthService () {
        return serviceLocator.getAuthService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }


    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += ": " + description;
        return result;
    }

}
