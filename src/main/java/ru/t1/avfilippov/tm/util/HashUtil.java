package ru.t1.avfilippov.tm.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {
    String SECRET = "123123";

    Integer ITERATION = 755;

    static String salt(final String value) {
        if (value== null) return null;
        String result = value;
        for (int i = 0; i< ITERATION; i++) {
            result = md5(SECRET+result+SECRET);
        }
        return result;
    }

    static String md5(final String value) {
        try {
            final MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] bytes = md.digest(value.getBytes());
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
